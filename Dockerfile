FROM python:3.7-alpine
#WORKDIR /minimal-flask-example
#ENV FLASK_APP=app.py
#ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache bash gcc musl-dev linux-headers
#COPY minimal-flask-example minimal-flask-example
#WORKDIR minimal-flask-example
#RUN pip install -r requirements.txt
RUN apk --no-cache add curl
#CMD ["/minimal-flask-example/run_app_dev.sh"]
#CMD ["/bin/bash"]
